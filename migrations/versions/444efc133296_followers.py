"""followers

Revision ID: 444efc133296
Revises: f4e2cc5deb87
Create Date: 2019-06-02 23:02:37.931093

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '444efc133296'
down_revision = 'f4e2cc5deb87'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('followers',
        sa.Column('follower_id', sa.Integer(), nullable=True),
        sa.Column('followed_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['followed_id'], ['users.id']),
        sa.ForeignKeyConstraint(['follower_id'], ['users.id'])
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('followers')
    # ### end Alembic commands ###
