from datetime import datetime
from app import db

class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    language = db.Column(db.String(5))

    def store(self):
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return '<Post %r>' % self.body

#Before made a new migration is necesary to do
#  flask db upgrade
# then the new migration
#  flask db migrate -m "posts table"
# or made the upgrade after made the