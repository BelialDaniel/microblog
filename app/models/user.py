import os
import jwt
import base64

from flask import url_for
from .paginated_api_mixin import PaginatedAPIMixin

from time import time
from hashlib import md5
from datetime import datetime, timedelta
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash # This import provides secured passwords

from flask import current_app
from app import db, login

followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('users.id'))
)

class User(PaginatedAPIMixin, UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password = db.Column(db.String(128))
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    #Higher level way to work with relationship
    #backref : add an author atribute to the Post it means post.author from a instance
    #lazy=dinamic : make this post to be a query without dinamic only be a simple list
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    # Token authentication scheme
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)

    # 
    followed=db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'),
        lazy='dynamic'
    )

    def __repr__(self):
        return '<User %r>' % self.username
        #return f'<User {self.usernpytame}>'
        #return '<User {} >'.format(sef.username)
    
    def store(self):
        db.session.add(self)
        db.session.commit()
    
    def set_password(self, password):
        self.password = generate_password_hash(password)
        db.session.commit()
    
    def check_password(self, password):
        return check_password_hash(self.password, password)
    
    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return f'https://gravatar.com/avatar/{digest}?d=identicon&size={size}'
    
    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            db.session.commit()

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            db.session.commit()

    def is_following(self, user):
        # Here the filter takes an expresion instead a keyword argument
        #  like done in filter_by 
        #  c is for lumn
        return self.followed.filter(
            followers.c.followed_id == user.id).count() == 1

    def followed_posts(self):
        from .post import Post
        # these are the post of the person that the user followed
        followed_posts = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
                followers.c.follower_id == self.id)
        # And here are aded the post of the user
        return followed_posts.union(self.posts).order_by(Post.timestamp.desc())

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)
    
    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'last_seen': self.last_seen.isoformat() + 'Z',
            'about_me': self.about_me,
            'post_count': self.posts.count(),
            'follower_count': self.followers.count(),
            'followed_count': self.followed.count(),
            '_links': {
                'self': url_for('api.get_user', id=self.id),
                'followers': url_for('api.get_followers', id=self.id),
                'followed': url_for('api.get_followed', id=self.id),
                'avatar': self.avatar(128)
            }
        }

        if include_email:
            data['email'] = self.email
        return data
    
    def from_dict(self, data, new_user=False):
        for field in ['username', 'email', 'about_me']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()

        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

#This function help us to know the user that is signed in the page
@login.user_loader
def load_user(id):
    return User.query.get(int(id))

# to store a user is necesary to call db.session.add and pass the instance of the model, 
# session represents an active session to add changes to the DB
# To apply all the changes in sesion  to the database is necessary to commit
# db.session.commit

#user.posts.all()
#post is a query because has the property dymamic
#user.query.order_by(User.sername.desc()) order the user
#db.session.delete(user|post)

# to update an user is necesary to retrieve the user from the DB
# then add to the db session and commit 