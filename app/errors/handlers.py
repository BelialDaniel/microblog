from flask import render_template, request
from app.errors import bp_errors

from app.api.errors import error_response as api_error_response

def wants_json_response():
    return request.accept_mimetypes['application/json'] >= request.accept_mimetypes['text/html']

@bp_errors.app_errorhandler(404)
def not_found_error(error):
    # in flask when is returned any response 
    #  we can add a status code as a second value as show below
    if wants_json_response():
        return api_error_response(404)
    return render_template('errors/404.html'), 404

@bp_errors.app_errorhandler(500)
def internal_error(error):
    # In some cases when is trhowed the error 500 
    #  the session could have information that is not commited yet
    from app import db
    db.session.rollback()
    if wants_json_response():
        return api_error_response(500)
    return render_template('errors/500.html'), 500
