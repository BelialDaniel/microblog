from flask import g

# The HTTPBasicAuth class from Flask-HTTPAuth is the one that 
#  implements the basic authentication flow. 
#  The two required functions are configured through the 
#  verify_password and error_handler decorators respectively. 
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth
from app.models import User
from .errors import error_response

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth()

@basic_auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username=username).first()
    if user is None:
        return False

    g.current_user = user
    return user.check_password(password)

@basic_auth.error_handler
def basic_auth_error():
    # The 401 error is defined in the HTTP standard as the "Unauthorized" error. 
    return error_response(401)

@token_auth.verify_token
def verify_token(token):
    g.current_user = User.check_token(token) if token else None
    return g.current_user is not None

@token_auth.error_handler
def token_auth_error():
    return error_response(401)