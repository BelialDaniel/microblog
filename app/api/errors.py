from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES

# This function uses the handy HTTP_STATUS_CODES dictionary from Werkzeug 
#  (a core dependency of Flask) that provides a short descriptive name for 
#  each HTTP status code.
def error_response(status_code, message=None):
    payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response

def bad_request(message):
    return error_response(400, message)