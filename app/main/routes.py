from datetime import datetime
from flask_babel import _, get_locale
from guess_language import guess_language
from flask_login import current_user, login_required
from flask import request, render_template, redirect, flash, url_for, g, jsonify, current_app

from app.main import bp_main
from .forms import EditProfileForm, PostForm

#This decorator helps to execute the method before
# each route request 
@bp_main.before_request
def before_request():
    from app import db
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.locale = str(get_locale())

@bp_main.route("/translate", methods=['POST'])
@login_required
def translate_text():
    from app.util.translate import translate_text
    return jsonify({
                'text': translate_text(
                            text=request.form['text'],
                            _from=request.form['from'],
                            to=request.form['to']
                        ) 
            })

@bp_main.route("/", methods=['GET', 'POST'])
@bp_main.route("/home", methods=['GET', 'POST'])
@login_required
def index():
    from app.models import Post

    form = PostForm()
    if form.validate_on_submit():
        language = guess_language(form.post.data)
        if language == 'UNKNOWN' or len(language) > 5:
            language = ''
        post = Post(body=form.post.data, author=current_user, language=language)
        post.store()
        flash('Your post is now live!')
        return redirect(url_for('main.index'))
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(
        page, # Number of the page
        int(current_app.config['POST_PER_PAGE']), # number of items pe page
        False # all the invalid pages requested return an empty lists 
    )

    next_url = url_for('main.index', page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.index', page=posts.prev_num) if posts.has_prev else None

    return render_template(
            'index.html', 
            title='Home', 
            form=form, 
            posts=posts.items,
            next_url=next_url,
            prev_url=prev_url 
        )

@bp_main.route('/explore')
@login_required
def explore():
    from app.models import Post

    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
            page, 
            int(current_app.config['POST_PER_PAGE']),
            False
        )

    next_url = url_for('main.explore', page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.explore', page=posts.prev_num) if posts.has_prev else None

    return render_template(
            'index.html', 
            title='Explore', 
            posts=posts.items, 
            next_url=next_url,
            prev_url=prev_url 
        )


@bp_main.route('/user/<username>')
def user(username):
    from app.models import User, Post

    # if the user did not found return error 404 to the user
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate (
            page,
            int(current_app.config['POST_PER_PAGE']),
            False
        )

    next_url = url_for('main.user', username=user.username, page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.user', username=user.username, page=posts.prev_num) if posts.has_prev else None

    return render_template(
            'user/user.html', 
            user=user, 
            posts=posts.items,
            next_url=next_url,
            prev_url=prev_url 
        )

@bp_main.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    from app import db

    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved')
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('user/edit.html', title='Edit profile', form=form)

@bp_main.route('/follow/<username>')
@login_required
def follow(username):
    from app.models import User

    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(f'User { username } not fount')
        redirect(url_for('main.home'))
    
    if user == current_user:
        flash(f'you can not follow youself!')
        return redirect(url_for('main.user', username = username))
    
    current_user.follow(user)
    flash(f'You are now following { username }')
    return redirect(url_for('main.user', username = username))

@bp_main.route('/unfollow/<username>')
@login_required
def unfollow(username):
    from app.models import User

    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(f'User { username } not fount')
        redirect(url_for('main.home'))
    
    if user == current_user:
        flash(f'you can not unfollow youself!')
        return redirect(url_for('main.user', username=username))
    
    current_user.unfollow(user)
    flash(f'You are now following { username }')
    return redirect(url_for('main.user', username=username))
