import click

@click.group()
def cli():
    """ D= """
    pass

@click.command()
@click.option('--users_count', default = 1, help = 'Seed user to the Database')
@click.option('--posts_count', default = 1, help = 'Seed post to the Database')
def all(users_count, posts_count):
    from user_seeder import UserSeeder
    from app import create_app

    app = create_app()
    with app.app_context():
        for index in range(users_count):
            user = UserSeeder.seed(index)

cli.add_command(all)
if __name__ == '__main__':
    cli()