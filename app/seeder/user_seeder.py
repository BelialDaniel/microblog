from faker import Faker

class UserSeeder:
    
    @classmethod
    def seed(cls, index):
        from app.models import User

        fake = Faker()

        user = User(username=fake.name(), email=fake.email(), about_me=fake.text(max_nb_chars=140))
        #user.set_password(fake.password())
        user.store()

        return user