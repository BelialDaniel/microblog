import os
import logging

from logging.handlers import SMTPHandler, RotatingFileHandler

from flask import Flask, request, current_app

from config import Config
from flask_mail import Mail
from flask_babel import Babel
from flask_moment import Moment
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
 
db = SQLAlchemy()
migrate = Migrate()
mail = Mail()
bootstrap = Bootstrap()
moment = Moment()
# This is not implemented yet
babel = Babel()

login = LoginManager()
# This line is used to protect routes to users not logged in, 
# all the others routes that need an user logged in 
# is necessary to implement the decorator @login_required
login.login_view = 'auth.login'
login.login_message = 'Please log in to access this page.'

def create_app(config_class = Config):
    app = Flask(__name__)
    app.config.from_object(config_class) #Here is imported the configuration when the app starts

    db.init_app(app)
    migrate.init_app(app, db)
    mail.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)
    babel.init_app(app)
    login.init_app(app)

    from app.errors import bp_errors
    app.register_blueprint(bp_errors)

    from app.auth import bp_auth
    app.register_blueprint(bp_auth, url_prefix='/auth')

    from app.main import bp_main
    app.register_blueprint(bp_main)

    from app.api import bp_api
    app.register_blueprint(bp_api, url_prefix='/api')

    # here is set up the email login
    if not app.debug and not app.testing: # if is in debug mode is not necessary to send the errors to the email
        app.logger.setLevel(logging.INFO)
        app.logger.info('Debug mode')

        if app.config['MAIL_SERVER']:
            auth = None
            if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
                auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
            
            secure = None
            if app.config['MAIL_USE_TLS']:
                secure = ()
            
            # Class from the python standar library 
            # that log the email addres 
            mail_handler = SMTPHandler(
                mailhost = (app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                fromaddr = 'no-reply@' + app.config['MAIL_SERVER'],
                toaddrs = app.config['ADMINS'], 
                subject = 'Microblog Failure',
                credentials = auth, 
                secure = secure
            )

            app.logger.setLevel(logging.INFO)
            app.logger.info('SMTPHandler startup')

        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

        # The aplication creates a logs directory 
        #  under microblog if it does not exist yet
        #  and create log file named microblog
        if not os.path.exists('logs'):
            os.mkdir('logs')
            app.logger.setLevel(logging.INFO)
            app.logger.info('logs directory created')
        
        file_handler = RotatingFileHandler('logs/microblog.log', maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
        
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('Microblog app created')    
    
    return app

# This method help us to select the best language from the request and then
#  translate our page
@babel.localeselector
def get_local():
    return request.accept_languages.best_match(current_app.config['LANGUAGES'])

#app was imported here to avoid circular dependencies 
from app.models import user, post
# to have an instance of the app just type in terminal : flask shell

#Debug mode
# To debug mode is necesari to add a variable :
# export FLASK_MODE=1
# this variable will display the error log in the browser