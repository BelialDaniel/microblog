from threading import Thread
from flask import current_app

def send_async_email(app, message):
    from app import mail
    with app.app_context():
        mail.send(message)

def send_email(subject, sender, recipients, text_body, html_body):
    from app import mail
    from flask_mail import Message

    message=Message(
        subject=subject, 
        sender=sender, 
        recipients=recipients, 
        body=text_body, 
        html=html_body
    )

    Thread(target=send_async_email, args=(current_app._get_current_object(), message)).start()