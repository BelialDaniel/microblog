import requests
import json

def translate_text(text, _from, to):
    url=f"https://translate.googleapis.com/translate_a/single"
    params=f"?client=gtx&sl={_from}&tl={to}&dt=t&q={text}"
    
    response=requests.get(url + params)
    
    if response is None or response.status_code != 200:
            return "Error : The translation service failed"
    
    json_response=json.loads(response.content.decode('utf-8-sig'))
    return json_response[0][0][0]