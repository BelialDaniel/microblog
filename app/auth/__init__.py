"""
This is the init for authentication (auth)
"""
from flask import Blueprint

bp_auth = Blueprint('auth', __name__)

from app.auth import routes
#now routes.py has only the routs that are related with the user authentication