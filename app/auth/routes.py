from flask import request, render_template, redirect, flash, url_for
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

from app.auth import bp_auth
from .forms import LoginForm, RegistrationForm, ResetPasswordRequestForm, ResetPasswordForm

from app.auth.email import send_password_reset_email

# Support to acepting dada from the user 
@bp_auth.route("/login", methods=['GET', 'POST'])
def login():
    from app.models import User

    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        
        if user is None or not user.check_password(form.password.data):
            flash("Invalid user or password")
            return redirect(url_for('auth.login'))
        
        login_user(user, remember=form.remember_me.data)
        # With this variable we check if the user not logged in tried to  access to another page, 
        # after log in we redirect the user to that page
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page =  url_for('main.index')
        return redirect(next_page)
    return render_template('user/login.html', title='Sign In', form=form)

@bp_auth.route('/register', methods=['GET', 'POST'])
def register():
    from app.models import User

    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password1.data)
        user.store()

        flash('User created')
        return redirect(url_for('auth.login'))
    
    return render_template('user/new.html', title='Register new user', form=form) 

@bp_auth.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@bp_auth.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        redirect(url_for('main.index'))
    
    from app.models import User
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for instructions to reset your email')
        return redirect(url_for('auth.login'))
    return render_template('user/reset_password_request.html', tittle='Reset password', form=form)

@bp_auth.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        redirect(url_for('main.index'))

    from app.models import User
    
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('main.index'))
    
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password1.data)
        flash('Your password has been reset')
        return redirect(url_for('auth.login'))
    return render_template('user/reset_password.html', form=form)
