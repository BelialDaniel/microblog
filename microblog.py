from app import create_app, db
from app.models import User, Post

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}

"""
In terminal: to tell flask where the aplication 
is located is necesary to define a local variable:
export FLASK_APP=microblog.py 
set FLASK_APP=microblog.py in windows

deactivate in terminal ends the app in flask
"""