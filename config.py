import os
from dotenv import load_dotenv

load_dotenv(dotenv_path='./.env')

# this variable is going to obtain location of the app
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    #Not hardcode the variables 
    """
    Runing in the terminal SECRET_KEY='anotherKey', set the secret key to the configuratiorn class

    defining variable in bash : export VARIABLE_EXAMPLE='BelialDaniel'

    """
    SECRET_KEY = os.getenv('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    #dialect+driver://username:password@host:port/database #this is my URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # This is the configuration to send the errors to the email
    MAIL_SERVER = os.getenv('MAIL_SERVER')
    MAIL_PORT = os.getenv('MAIL_PORT', 25)
    MAIL_USE_TLS = os.getenv('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.getenv('MAIL_USERNAME')
    MAIL_PASSWORD = os.getenv('MAIL_PASSWORD')
    ADMINS = [os.getenv('EMAIL')]

    POST_PER_PAGE = os.getenv('POST_PER_PAGE')

    LANGUAGES = ['en', 'es']
#the third slash (/) means that is skipping the hostname because is going to use a base in file